from django.contrib import admin
from .models import (
    Student,
    Department,
    Image,
    Marksheet,
    TempImage
)

admin.site.register(Student)
admin.site.register(Department)
admin.site.register(Image)
admin.site.register(Marksheet)
admin.site.register(TempImage)