from django.shortcuts import render
from django.core.mail import EmailMessage as Em
from .forms import ImageForm
from django.urls import reverse, reverse_lazy
from .models import Image, Student, TempImage, Marksheet, Department
from django.views.generic import TemplateView
import pytesseract, re, cv2, os
from PIL import Image as I

def mail_staff(store_data):
    register_number = store_data.student_reg.student_reg
    register_name = store_data.student_reg.student_name
    register_mail = store_data.student_reg.student_mail
    semester = store_data.student_sem
    image = store_data.image.path

    department = store_data.student_reg.student_dept.department
    to_mail = Department.objects.get(department=department)
    to_mail = to_mail.hod_mail
    print(register_number, register_name, register_mail, semester, department, to_mail)
    subject = "Certificate Verification - {}".format(register_number)
    body = "Hello Sir/Madam,\n\tThe latest certificate for verification is attached below:\n\tThe info are:\n\t\tStudent Name: {} \n\t\tRegister Number: {}\n\t\tSemester: {}\n\t\tContact mail: {}".format(register_name, register_number, semester.title(), register_mail)

    try:
        email = Em(subject=subject, body=body, 
                   to=[to_mail])
        email.attach_file(image)
        email.send()
    except ConnectionRefusedError:
        return False
    else:
        return True


def verify_ocr(id):
    '''
    Reads text from the certificate.
    '''
    stored_image = TempImage.objects.get(id=id)
    stored_image = stored_image.image_data.path
    image = cv2.imread(stored_image)
    image = cv2.resize(image, (898, 1280))

    image = image[100:200, 290:2000]
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    filename = "{}.png".format(os.getpid())
    cv2.imwrite(filename, gray)

    text = pytesseract.image_to_string(I.open(filename))
    os.remove(filename)
    text = text.split("\n")
    for i in text:
        txt = i
        x = re.search("S...N..", txt)
        y = re.search("S..N..", txt)
        if x is not None:
            txt = txt.split(" ")
            txt = txt[-1]
            print(txt)
            return txt
        if y is not None:
            txt = txt.split(" ")
            txt = txt[-1]
            print(txt)
            return txt
    return False

def get_dept(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        image = request.FILES['image']
        if form.is_valid():
            stud_reg = form.cleaned_data.get('student_reg')
            stud_sem = form.cleaned_data.get('student_sem')
            try:
                reg = Student.objects.get(student_reg=stud_reg)
            except(KeyError, Student.DoesNotExist):
                form = ImageForm()
                context = {
                    'error_message': 'Enter valid Register Number.',
                    'form': form,
                }
                return render(request, 'home.html', context)
            else:
                dept = reg.student_dept.department
                temp_image = TempImage(image_data=image)
                temp_image.save()
                status = verify_ocr(temp_image.id)
                if status:
                    store_data = Image(image=image, student_reg=reg, student_dept=dept, student_sem=stud_sem, serial_num=status)
                    store_data.save()
                    try:
                        marksheet_data = Marksheet.objects.get(student_reg=reg, marksheet_semester=stud_sem)
                    except(KeyError, Marksheet.DoesNotExist):
                        form = ImageForm()
                        context = {
                            'error_msg': 'Records for chosen semester does not exist.',
                            'form': form,
                        }
                        return render(request, 'home.html', context)
                    else:
                        if marksheet_data.serial_number == status:
                            if mail_staff(store_data):
                                context = {
                                    'form': form,
                                    'success_msg': 'Your certificate has been submitted successfully',
                                    'contact': reg,
                                }
                            else:
                                context = {
                                    'form': form,
                                    'error_msg': 'Network Error, try again in some time.',
                                }
                        else:
                            context = {
                                'form': form,
                                'error_msg': 'Your certificate records not found, try again with a clear picture of certificate.',
                            }
                else:
                    context = {
                        'form': form,
                        'error_msg': 'Invalid certificate.',
                    }
                return render(request, 'home.html', context)
    else:
        form = ImageForm()
        return render(request, 'home.html', {'form':form})
        
    return render(request, 'home.html', {'form':form})

class ProcessView(TemplateView):
    template_name = "process.html"
