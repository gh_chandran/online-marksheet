from django.urls import path
from .views import get_dept, ProcessView#, add_success

urlpatterns = [
    path('', get_dept, name='home'),
    path('process/', ProcessView.as_view(), name='process'),
    # path('detail/<int:store_data_id>', add_success, name='detail'),
]
