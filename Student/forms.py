from django.forms import Form, TextInput, ImageField
from .models import Image
from django import forms
from crispy_forms.helper import FormHelper

class ImageForm(Form):
    student_reg = forms.CharField(
        label="Register Number: ", max_length=9, widget=TextInput(attrs={'class': 'form-input'}))
    sem = [
        ('one', 'One'),
        ('two', 'Two'),
        ('three', 'Three'),
        ('four', 'Four'),
        ('five', 'Five'),
        ('six', 'Six'),
        ('seven', 'Seven'),
        ('eight', 'Eight'),
    ]
    student_sem = forms.CharField(max_length=6, label="Semester of the certificate: ", 
        widget=forms.Select(choices=sem, attrs={'class': 'form-input'})
    )
    image = forms.ImageField(label='Image of the certificate: ')

    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
