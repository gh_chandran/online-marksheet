from django.db import models

class Department(models.Model):
    depts = [
        ('Civil', 'Civil Engineering'),
        ('CSE', 'Computer Science and Engineering'),
        ('ECE', 'Electronics and Communication Engineering'),
        ('EEE', 'Electrical and Electronics Engineering'),
        ('EIE', 'Electrical and Instrumentation Engineering'),
        ('IBT', 'Industrial Biotech Engineering'),
        ('IT', 'Information Technology'),
        ('Mech', 'Mechanical Engineering'),
        ('Prod', 'Production Engineering')
    ]

    department = models.CharField(max_length=6, choices=depts)
    hod_name = models.CharField(max_length=254)
    hod_mail = models.EmailField(max_length=254, default='hod@mail.com')

    def __str__(self):
        return self.department

class Student(models.Model):
    student_name = models.CharField(max_length=254)
    student_reg = models.CharField(max_length=8, unique=True)
    student_mail = models.EmailField(max_length=254)
    student_dept = models.ForeignKey(to=Department, on_delete=models.CASCADE)
    year = [
        ('one', 'First Year'),
        ('two', 'Second Year'),
        ('three', 'Third Year'),
        ('four', 'Fourth Year'),
    ]
    student_year = models.CharField(max_length=6, choices=year)

    def __str__(self):
        return self.student_reg

class Marksheet(models.Model):
    student_reg = models.ForeignKey(to=Student, on_delete=models.CASCADE)
    serial_number = models.CharField(max_length=7)
    sem = [
        ('one', 'One'),
        ('two', 'Two'),
        ('three', 'Three'),
        ('four', 'Four'),
        ('five', 'Five'),
        ('six', 'Six'),
        ('seven', 'Seven'),
        ('eight', 'Eight'),
    ]
    marksheet_semester = models.CharField(max_length=7, choices=sem)

    def __str__(self):
        return self.serial_number

class Image(models.Model):
    depts = [
        ('Civil', 'Civil Engineering'),
        ('CSE', 'Computer Science and Engineering'),
        ('ECE', 'Electronics and Communication Engineering'),
        ('EEE', 'Electrical and Electronics Engineering'),
        ('EIE', 'Electrical and Instrumentation Engineering'),
        ('IBT', 'Industrial Biotech Engineering'),
        ('IT', 'Information Technology'),
        ('Mech', 'Mechanical Engineering'),
        ('Prod', 'Production Engineering')
    ]
    image = models.ImageField(upload_to='%d')
    student_dept = models.CharField(max_length=10, choices=depts)
    student_reg = models.ForeignKey(to=Student, on_delete=models.CASCADE)
    serial_num = models.CharField(max_length=6, default='00000')
    sem = [
        ('one', 'One'),
        ('two', 'Two'),
        ('three', 'Three'),
        ('four', 'Four'),
        ('five', 'Five'),
        ('six', 'Six'),
        ('seven', 'Seven'),
        ('eight', 'Eight'),
    ]
    student_sem = models.CharField(max_length=7, choices=sem)

    def __str__(self):
        return self.student_reg.student_reg + "'s Sem: " + self.student_sem.title()

class TempImage(models.Model):
    image_data = models.ImageField(upload_to='temp/%d')
    
    def __str__(self):
        return image_data.url